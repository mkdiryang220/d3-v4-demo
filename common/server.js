var http = require("http");
var url = require("url");


var fs = require("fs");
var data = fs.readFileSync('demo11.html');
// var dataCSV = fs.readFileSync('demo5-data.csv');

function start(route) {
    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;
        console.log("Request for " + pathname + " received.");

        route(pathname);

        response.writeHead(200, { "Content-Type": "text/html" });
        response.write(data);
        response.end();
    }

    http.createServer(onRequest).listen(8888);
    console.log("Server has started.");
}

exports.start = start;