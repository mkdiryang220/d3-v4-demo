var x = d3.scaleTime()
    .domain([new Date(opt.starttime), new Date(opt.endtime) - 1])
    .rangeRound([0, width]);
console.log(width, height)

// add X axis
let xAxis = d3.axisBottom(x)
    .ticks(d3.timeDay)
    .tickPadding(0)

// 纵坐标
let y = d3.scaleLinear().range([height, 0])
// let yScale = y.domain([0, d3.max(data, function(d){return d.value})])

// 画布容器
var zoom = d3.zoom()
    .translateExtent([[0, 0], [width, height]])
    .extent([[0, 0], [width, height]])
    .scaleExtent([0.1, 10])
    .on('zoom', zoomed)
    .on("start", zoomStart)
    .on("end", zoomEnd);
var svg = d3.select("#grid").append("svg")
    .attr("class", "svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .call(zoom)

// svg.append("g")
//     .attr("class", "axis axis--grid")
//     .attr("transform", "translate(0," + height + ")")
//     .call(d3.axisBottom(x)
//         .ticks(d3.timeHour, 12)
//         .tickSize(-height)
//         .tickFormat(function() { return null; }))
//     .selectAll(".tick")
//     .classed("tick--minor", function(d) { return d.getHours(); });

// x轴显示
// let globalX = d3.select("#grid")
//     .append('svg')
//     .attr("class", "axis axis--x")
//     .attr("width", width + margin.left + margin.right)
//     .attr("transform", `translate(${margin.left},${height+margin.top})`)
//     .call(xAxis)

// 在chart上选取时间的遮罩层
svg.append("g")
    .attr("class", "brush")
    .call(d3.brushX()
        .extent([[0, 0], [width, height]])
        .on("brush", brushed));